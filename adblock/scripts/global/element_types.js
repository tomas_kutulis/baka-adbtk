var ElementTypes = {
  NONE: 0,
  script: 1,
  image: 2,
  'object': 4,
  subdocument: 8,
  'document': 16,
  popup: 32,

  DEFAULT_TYPES: 15
};