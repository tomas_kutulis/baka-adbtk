function setDefault(obj, value, defaultValue) {
  if (obj[value] === undefined) {
    obj[value] = defaultValue;
  }
  return obj[value];
}

function parseUri(url) {
  var matches = /^(([^:]+(?::|$))(?:(?:\w+:)?\/\/)?(?:[^:@\/]*(?::[^:@\/]*)?@)?(([^:\/?#]*)(?::(\d*))?))((?:[^?#\/]*\/)*[^?#]*)(\?[^#]*)?(\#.*)?/.exec(url);
  var uri = {};
  if (matches) {
    var keys = ['href', 'origin', 'protocol', 'host', 'hostname', 'port', 'pathname', 'search', 'hash'];
    for (var i = 0; i < keys.length; i++) {
      uri[keys[i]] = matches[i] || '';
    }
  }

  return uri;
}