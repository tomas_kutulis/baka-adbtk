(function () {
  function removeFrame(el) {
    var parentEl = el.parentNode;
    var cols = ((parentEl.getAttribute('cols') || '').indexOf(',') > 0);
    if (!cols && (parentEl.getAttribute('rows') || '').indexOf(',') <= 0) {
      return;
    }
    var index = 0;
    while (el.previousElementSibling) {
      index++;
      el = el.previousElementSibling;
    }
    var attr = (cols ? 'cols' : 'rows');
    var sizes = parentEl.getAttribute(attr).split(',');
    sizes[index] = '0';
    parentEl.setAttribute(attr, sizes.join(','));
  }

  function destroyElement(el) {
    if (el.nodeName === 'FRAME') {
      removeFrame(el);
    } else {
      el.style.setProperty('display', 'none', 'important');
    }
  }

  function block_list_via_css(selectors) {
    if (!selectors.length) {
      return;
    }

    var css_chunk = insertCssChunk();

    function fill_in_css_chunk(GROUPSIZE) {
      if (!css_chunk.sheet) {
        window.setTimeout(fill_in_css_chunk, 0);
        return;
      }

      for (var i = 0; i < selectors.length; i += GROUPSIZE) {
        var line = selectors.slice(i, i + GROUPSIZE);
        var rule = line.join(',') + ' { display:none !important; }';
        css_chunk.sheet.insertRule(rule, 0);
      }
    }

    function insertCssChunk() {
      var css_chunk = document.createElement('style');
      css_chunk.type = 'text/css';
      css_chunk.id = 'AdBTK_css';
      (document.head || document.documentElement).insertBefore(css_chunk, null);

      return css_chunk;
    }

    fill_in_css_chunk(1000);
  }

  function adblock_begin(startPurger) {
    if (!(document.documentElement instanceof HTMLElement))
      return;

    startPurger();

    get_content_script_data(document.location.hostname, function (selectors) {
      block_list_via_css(selectors);
    });
  }

  function get_content_script_data(domain, callback) {
    chrome.extension.sendRequest({command: 'call', domain: domain}, callback);
  }

  function getSelector(tag, src) {
    var attr = (tag === 'OBJECT' ? 'data' : 'src');
    return tag + '[' + attr + src.op + '"' + src.text + '"]';
  }

  function getTags() {
    var tags = {};
    tags[ElementTypes.image] = { IMG: 1 };
    tags[ElementTypes.subdocument] = { IFRAME: 1, FRAME: 1 };
    tags[ElementTypes.object] = { 'OBJECT': 1, EMBED: 1 };

    return tags;
  }

  var elementPurger = {
    onPurgeRequest: function (request) {
      if (request.command === 'purge-elements' && request.frameUrl === document.location.href.replace(/#.*$/, '')) {
        elementPurger._purgeElements(request);
      }
    },
    _purgeElements: function (request, lastTry) {
      var srcdata = this._srcsFor(request.url), tags = getTags()[request.elType];

      for (var i = 0; i < srcdata.length; i++) {
        for (var tag in tags) {
          var results = document.querySelectorAll(getSelector(tag, srcdata[i]));
          if (results.length) {
            for (var j = 0; j < results.length; j++) {
              destroyElement(results[j]);
            }
            return;
          }
        }
      }

      if (!lastTry) {
        var that = this;
        setTimeout(function () {
          that._purgeElements(request, true);
        }, 2000);
      }
    },
    _srcsFor: function (url) {
      var url_parts = parseUri(url), page_parts = this._page_location;
      var results = [];
      results.push({ op: '$=', text: url.match(/\:(\/\/.*)$/)[1] });
      if (url_parts.hostname === page_parts.hostname) {
        results.push({ op: '=', text: url_parts.pathname + url_parts.search + url_parts.hash });
        pushRelativeUrls(url_parts, page_parts, results);
      }

      return results;
    },
    _page_location: document.location
  };

  function pushRelativeUrls(url_parts, page_parts, results) {
    var url_dirs = url_parts.pathname.split('/'), page_dirs = page_parts.pathname.split('/');
    var i = getFirstDifferentPosition(url_dirs, page_dirs);
    var dir = new Array(page_dirs.length - i).join('../');
    var path = url_dirs.slice(i).join('/') + url_parts.search + url_parts.hash;
    if (dir) {
      results.push({ op: '$=', text: dir + path });
    } else {
      results.push({ op: '=', text: path });
      results.push({ op: '=', text: './' + path });
    }
  }

  function getFirstDifferentPosition(url_dirs, page_dirs) {
    var i = 0;
    while (page_dirs[i] === url_dirs[i] && i < page_dirs.length - 1 && i < url_dirs.length - 1) {
      i++;
    }
    return i;
  }

  adblock_begin(function () {
    chrome.extension.onRequest.addListener(elementPurger.onPurgeRequest);
  });
})();