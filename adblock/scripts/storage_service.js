'use strict';

angular.module('adbtk')
  .factory('storageService', function () {
    function getObject(key) {
      return JSON.parse(localStorage.getItem(key));
    }

    function setItem(key, value) {
      localStorage.setItem(key, JSON.stringify(value));
    }

    return  {
      getObject: getObject,
      setItem: setItem
    }
  });
