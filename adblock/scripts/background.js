'use strict';

angular.module('adbtk', [])
  .controller('MainCtrl', function (storageService, myFilters, frameData, backgroundUtils) {
    function onBeforeRequestHandler(details) {
      if (!frameData.track(details)) {
        return { cancel: false };
      }

      var tabId = details.tabId;
      var reqType = backgroundUtils.normalizeRequestType(details);
      var requestingFrameId = (reqType === 'sub_frame' ? details.parentFrameId : details.frameId);
      var frameDomain = frameData.get(tabId, requestingFrameId).domain;
      var elType = backgroundUtils.getElementTypeOnBeforeRequest(reqType);
      var blocked = myFilters.getBlocking().matches(details.url, elType, frameDomain);

      if (blocked && backgroundUtils.canPurge(elType)) {
        var frameUrl = frameData.get(tabId, requestingFrameId).url.replace(/#.*$/, '');
        var data = { command: 'purge-elements', tabId: tabId, frameUrl: frameUrl, url: details.url, elType: elType };
        chrome.tabs.sendRequest(tabId, data);
      }

      if (blocked && elType === ElementTypes.subdocument) {
        return { redirectUrl: 'about:blank' };
      }
      return { cancel: blocked };
    }

    function onCreatedNavigationTargetHandler(details) {
      var opener = frameData.get(details.sourceTabId, details.sourceFrameId);
      if (opener === undefined) {
        return;
      }

      if (details.url === 'about:blank') {
        details.url = opener.url;
      }

      if (myFilters.getBlocking().matches(details.url, ElementTypes.popup, opener.domain)) {
        chrome.tabs.remove(details.tabId);
      }
    }

    function get_content_script_data(domain) {
      return myFilters.getHiding() ? myFilters.getHiding().filtersFor(domain) : [];
    }

    function handleEarlyOpenedTabs (tabs) {
      for (var i = 0; i < tabs.length; i++) {
        var currentTab = tabs[i], tabId = currentTab.id;
        if (!frameData.get(tabId)) {
          frameData.track({url: currentTab.url, tabId: tabId, type: 'main_frame'});
        }
      }
    }


    chrome.webNavigation.onTabReplaced.addListener(function (details) {
      frameData.removeTabId(details.replacedTabId);
    });

    chrome.webNavigation.onHistoryStateUpdated.addListener(function (details) {
      if (backgroundUtils.correctOnHistoryStateUpdatedDetails()) {
        var tabData = frameData.get(details.tabId, details.frameId);
        if (tabData && tabData.url !== details.url) {
          details.type = 'main_frame';
          frameData.track(details);
        }
      }
    });

    chrome.extension.onRequest.addListener(function (request, sender, sendResponse) {
      if (request.command === 'call') {
        sendResponse(get_content_script_data(request.domain));
      }
    });

    chrome.webRequest.onBeforeRequest.addListener(onBeforeRequestHandler, { urls: ['http://*/*', 'https://*/*'] }, ['blocking']);

    chrome.tabs.onRemoved.addListener(frameData.removeTabId);

    chrome.webNavigation.onCreatedNavigationTarget.addListener(onCreatedNavigationTargetHandler);

    chrome.tabs.query({url: 'http://*/*'}, handleEarlyOpenedTabs);
    chrome.tabs.query({url: 'https://*/*'}, handleEarlyOpenedTabs);
  });