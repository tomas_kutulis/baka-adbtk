'use strict';

angular.module('adbtk')
  .factory('frameData', function () {
    var frameData = {};

    function initFrameDataTab(tabId) {
      if (!frameData[tabId]) {
        frameData[tabId] = {};
      }
    }

    function record(tabId, frameId, url) {
      initFrameDataTab(tabId);
      frameData[tabId][frameId] = { url: url, domain: parseUri(url).hostname };
    }

    function recordEmptyFrame(tabId, details) {
      var potentialEmptyFrameId = (details.type === 'sub_frame' ? details.parentFrameId : details.frameId);
      if (get(tabId, potentialEmptyFrameId) === undefined) {
        record(tabId, potentialEmptyFrameId, get(tabId, 0).url);
      }
    }

    function recordSubFrame(tabId, details) {
      if (details.type === 'sub_frame') {
        record(tabId, details.frameId, details.url);
      }
    }

    function get(tabId, frameId) {
      return frameId !== undefined ? (frameData[tabId] || {})[frameId] : frameData[tabId];
    }

    function track(details) {
      var tabId = details.tabId;

      if (tabId === -1) {
        return false;
      }

      if (details.type === 'main_frame') {
        removeTabId(tabId);
        record(tabId, 0, details.url);
        return true;
      }

      if (!frameData[tabId]) {
        return false;
      }

      recordEmptyFrame(tabId, details);
      recordSubFrame(tabId, details);

      return true;
    }

    function removeTabId(tabId) {
      delete frameData[tabId];
    }


    return {
      get: get,
      track: track,
      removeTabId: removeTabId
    }
  });