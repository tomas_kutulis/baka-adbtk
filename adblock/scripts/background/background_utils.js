'use strict';

angular.module('adbtk')
  .factory('backgroundUtils', function () {
    function getOtherRequestType(url) {
      if (url && url.pathname) {
        var pos = url.pathname.lastIndexOf('.');
        if (pos > -1) {
          if (['.ico', '.png', '.gif', '.jpg', '.jpeg' ,'.webp'].indexOf(url.pathname.slice(pos)) !== -1) {
            return 'image';
          }
        }
      }
      return 'object';
    }

    function normalizeRequestType(details) {
      return details.type === 'other' ? getOtherRequestType(parseUri(details.url)) : details.type;
    }

    function canPurge(elType) {
      return elType & (ElementTypes.image | ElementTypes.subdocument | ElementTypes.object);
    }

    function correctOnHistoryStateUpdatedDetails(details) {
      return details && details.hasOwnProperty('frameId') && details.hasOwnProperty('tabId') &&
        details.hasOwnProperty('url') && details.hasOwnProperty('transitionType') && details.transitionType === 'link';
    }

    function getElementTypeOnBeforeRequest(type) {
      switch (type) {
        case 'main_frame': return ElementTypes.document;
        case 'sub_frame': return ElementTypes.subdocument;
        default: return ElementTypes[type];
      }
    }

    return {
      normalizeRequestType: normalizeRequestType,
      canPurge: canPurge,
      correctOnHistoryStateUpdatedDetails: correctOnHistoryStateUpdatedDetails,
      getElementTypeOnBeforeRequest: getElementTypeOnBeforeRequest
    }
  });