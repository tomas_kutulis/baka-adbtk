'use strict';

angular.module('adbtk')
  .constant('FilterOptions', {
    NONE: 0,
    THIRDPARTY: 1,
    MATCHCASE: 2,
    FIRSTPARTY: 4
  })
  .factory('PatternFilter', function (Filter, FilterOptions) {
    var PatternFilter = function () {
      Filter.call(this);
    };

    PatternFilter.prototype = {
      __proto__: Filter.prototype,
      matches: function (url, elementType, isThirdParty) {
        if (!(elementType & this._allowedElementTypes) ||
          !isThirdParty && (this._options & FilterOptions.THIRDPARTY) ||
          isThirdParty && (this._options & FilterOptions.FIRSTPARTY) ||
          this._key && !this._key.test(url)) {
          return false;
        }

        return this._rule.test(url);
      }
    };

    function getPatternFilter(data) {
      var result = new PatternFilter();
      result._domains = Filter.toDomainSet(data.domainText, '|');
      result._allowedElementTypes = data.allowedElementTypes;
      result._options = data.options;
      result._rule = data.rule;
      result._key = data.key;
      return result;
    }

    function fromText(text) {
      return getPatternFilter(parseRule(text));
    }

    function parseRule(text) {
      var result = initParseRuleResult();
      var ruleAndOptions = getRuleAndOptionsFromText(text), rule = ruleAndOptions[0], options = ruleAndOptions[1];

      var allowedElementTypes;
      for (var i = 0; i < options.length; i++) {
        var option = options[i];

        if (/^domain\=/.test(option)) {
          result.domainText = option.substring(7);
          continue;
        }

        var inverted = (option[0] === '~');

        option = fixOption(option, inverted);

        if (option in ElementTypes) {
          allowedElementTypes = getAllowedElementTypes(allowedElementTypes, option, inverted);
        } else if (option === 'third_party') {
          result.options |= (inverted ? FilterOptions.FIRSTPARTY : FilterOptions.THIRDPARTY);
        } else if (option === 'match_case') {
          result.options |= FilterOptions.MATCHCASE;
        } else {
          allowedElementTypes = inverted ? ElementTypes.DEFAULT_TYPES : ElementTypes.NONE;
        }
      }

      result.allowedElementTypes = allowedElementTypes === undefined ? ElementTypes.DEFAULT_TYPES : allowedElementTypes;

      var matchcase = (result.options & FilterOptions.MATCHCASE) ? '' : 'i';
      if (/^\/.+\/$/.test(rule)) {
        result.rule = rule.substr(1, rule.length - 2); // remove slashes
        result.rule = new RegExp(result.rule, matchcase);
        return result;
      }

      setKey(result, rule, matchcase);

      result.rule = new RegExp(fixRegExp(rule), matchcase);

      return result;
    }

    function initParseRuleResult() {
      return {
        domainText: '',
        options: FilterOptions.NONE
      };
    }

    function getRuleAndOptionsFromText(text) {
      var optionsText = text.match(/\$~?[\w\-]+(?:=[^,\s]+)?(?:,~?[\w\-]+(?:=[^,\s]+)?)*$/);
      return optionsText ? [text.replace(optionsText[0], ''), optionsText[0].substring(1).toLowerCase().split(',')] : [text, []];
    }

    function fixOption(option, inverted) {
      if (inverted) {
        option = option.substring(1);
      }
      option = option.replace(/\-/, '_');
      if (option === 'object_subrequest') {
        option = 'object';
      } else if (option === 'background') {
        option = 'image';
      }

      return option;
    }

    function getAllowedElementTypes(allowedElementTypes, option, inverted) {
      if (inverted) {
        if (allowedElementTypes === undefined) {
          allowedElementTypes = ElementTypes.DEFAULT_TYPES;
        }
        allowedElementTypes &= ~ElementTypes[option];
      } else {
        if (allowedElementTypes === undefined) {
          allowedElementTypes = ElementTypes.NONE;
        }
        allowedElementTypes |= ElementTypes[option];
      }

      return allowedElementTypes;
    }

    function setKey(result, rule, matchcase) {
      var key = rule.match(/[\w&=]{5,}/);
      if (key) {
        result.key = new RegExp(key, matchcase);
      }
    }

    function fixRegExp(rule) {
      rule = rule.replace(/\*-\*-\*-\*-\*/g, '*');
      rule = rule.replace(/\*\*+/g, '*');
      rule = rule.replace(/([^a-zA-Z0-9_\|\^\*])/g, '\\$1');
      rule = rule.replace(/\^/g, '[^\\-\\.\\%a-zA-Z0-9_]');
      rule = rule.replace(/\*/g, '.*');
      rule = rule.replace(/^\|\|/, '^[^\\/]+\\:\\/\\/([^\\/]+\\.)?');
      rule = rule.replace(/^\|/, '^');
      rule = rule.replace(/\|$/, '$');
      rule = rule.replace(/\|/g, '\\|');
      rule = rule.replace(/^\.\*/, '');
      rule = rule.replace(/\.\*$/, '');

      return rule;
    }

    return {
      fromText: fromText
    };
  });