'use strict';

angular.module('adbtk')
  .factory('FilterSet', function (DomainSet) {
    function FilterSet() {
      this.items = { 'global': [] };
      this.exclude = {};
    }

    FilterSet.prototype = {
      _viewFor: function (domain) {
        var result = new FilterSet();
        result.items['global'] = this.items['global'];
        for (var nextDomain in DomainSet.domainAndParents(domain)) {
          if (this.items[nextDomain]) {
            result.items[nextDomain] = this.items[nextDomain];
          }
          if (this.exclude[nextDomain]) {
            result.exclude[nextDomain] = this.exclude[nextDomain];
          }
        }
        return result;
      },
      filtersFor: function (domain) {
        var data = {}, limited = this._viewFor(domain);
        setFiltersItems(data, limited);
        subtractFiltersExcluded(data, limited);
        return getFiltersSelectors(data);
      },
      matches: function (url, elementType, frameDomain, isThirdParty) {
        var limited = this._viewFor(frameDomain);
        for (var k in limited.items) {
          var entry = limited.items[k];
          for (var i = 0; i < entry.length; i++) {
            var filter = entry[i];
            if (!filter.matches(url, elementType, isThirdParty)) {
              continue;
            }
            var excluded = false;
            for (var k2 in limited.exclude) {
              if (limited.exclude[k2][filter.id]) {
                excluded = true;
                break;
              }
            }
            if (!excluded) {
              return true;
            }
          }
        }

        return false;
      }
    };

    function setFiltersItems(data, limited) {
      for (var subdomain in limited.items) {
        var entry = limited.items[subdomain];
        for (var i = 0; i < entry.length; i++) {
          var filter = entry[i];
          data[filter.id] = filter;
        }
      }
    }

    function subtractFiltersExcluded(data, limited) {
      for (var subdomain in limited.exclude) {
        for (var filterId in limited.exclude[subdomain]) {
          delete data[filterId];
        }
      }
    }

    function getFiltersSelectors(data) {
      var result = [];
      for (var k in data) {
        result.push(data[k].selector);
      }
      return result;
    }

    function fromFilters(data) {
      var result = new FilterSet();

      for (var _ in data) {
        var filter = data[_];
        for (var d in filter._domains.has) {
          if (filter._domains.has[d]) {
            var key = (d === DomainSet.ALL ? 'global' : d);
            setDefault(result.items, key, []).push(filter);
          } else if (d !== DomainSet.ALL) {
            setDefault(result.exclude, d, {})[filter.id] = true;
          }
        }
      }

      return result;
    }

    return {
      fromFilters: fromFilters
    };
  });