'use strict';

angular.module('adbtk')
  .factory('SelectorFilter', function (Filter) {
    var SelectorFilter = function(text) {
      Filter.call(this);

      if (text) {
        var parts = text.match(/(^.*?)\#\@?\#(.+$)/);
        this._domains = Filter.toDomainSet(parts[1], ',');
        this.selector = parts[2];
      }
    };

    SelectorFilter.merge = function(filter, excludeFilters) {
      if (!excludeFilters) {
        return filter;
      }

      var result = new SelectorFilter();
      result.selector = filter.selector;
      result._domains = getSubtractedDomains(filter, excludeFilters);

      return result;
    };

    SelectorFilter.prototype = {
      __proto__: Filter.prototype
    };

    function getSubtractedDomains(filter, excludeFilters) {
      var domains = filter._domains.clone();
      for (var i = 0; i < excludeFilters.length; i++) {
        domains.subtract(excludeFilters[i]._domains);
      }
      return domains;
    }

    return SelectorFilter;
  });