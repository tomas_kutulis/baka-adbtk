'use strict';

angular.module('adbtk')
  .factory('filterNormalizer', function () {
    function getFilters(text) {
      var result = [], lines = text.split('\n');
      for (var i = 0; i < lines.length; i++) {
        if (!textIsComment(lines[i])) {
          result.push(lines[i]);
        }
      }
      return result.join('\n') + '\n';
    }

    function textIsComment(text) {
      return text.length === 0 || text[0] === '!' || (/^\[adblock/i.test(text)) || (/^\(adblock/i.test(text));
    }

    return {
      getFilters: getFilters
    }
  });