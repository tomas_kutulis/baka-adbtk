'use strict';

angular.module('adbtk')
  .factory('BlockingFilterSet', function () {
    function BlockingFilterSet(patternFilterSet, whitelistFilterSet) {
      this.patternFilterSet = patternFilterSet;
      this.whitelistFilterSet = whitelistFilterSet;
      this._matchCache = {};
    }

    BlockingFilterSet.prototype = {
      matches: function (url, elementType, frameDomain) {
        var isThirdParty = checkThirdParty(parseUri(url).hostname, frameDomain);
        var key = url + ' ' + elementType + ' ' + isThirdParty;
        if (key in this._matchCache) {
          return this._matchCache[key];
        }

        this._matchCache[key] = this._getIfBlock(url, elementType, frameDomain, isThirdParty);
        return this._matchCache[key];
      },
      _getIfBlock: function (url, elementType, frameDomain, isThirdParty) {
        return this.whitelistFilterSet.matches(url, elementType, frameDomain, isThirdParty) ?
          false : this.patternFilterSet.matches(url, elementType, frameDomain, isThirdParty);
      }
    };

    function checkThirdParty(domain1, domain2) {
      return secondLevelDomainOnly(domain1) !== secondLevelDomainOnly(domain2);
    }

    function secondLevelDomainOnly(domain) {
      var match = domain ? domain.match(/([^\.]+\.(?:co\.)?[^\.]+)\.?$/) : '';
      return (match ? match[1] : domain).toLowerCase();
    }

    return BlockingFilterSet;
  });