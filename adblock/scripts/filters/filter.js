'use strict';

angular.module('adbtk')
  .factory('Filter', function (DomainSet) {
    var Filter = function () {
      this.id = ++Filter._lastId;
    };

    Filter._lastId = 0;

    Filter.isSelectorFilter = function (text) {
      return /\#\@?\#./.test(text);
    };

    Filter.isSelectorExcludeFilter = function (text) {
      return /\#\@\#./.test(text);
    };

    Filter.isWhitelistFilter = function (text) {
      return /^\@\@/.test(text);
    };

    Filter.toDomainSet = function (domainText, divider) {
      var domains = domainText.split(divider);

      var data = {};
      data[DomainSet.ALL] = true;

      if (domains != '') {
        for (var i = 0; i < domains.length; i++) {
          var domain = domains[i];
          if (domain[0] === '~') {
            data[domain.substring(1)] = false;
          } else {
            data[domain] = true;
            data[DomainSet.ALL] = false;
          }
        }
      }

      return new DomainSet(data);
    };

    return Filter;
  });