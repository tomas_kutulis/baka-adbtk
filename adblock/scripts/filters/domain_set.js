'use strict';

angular.module('adbtk')
  .factory('DomainSet', function () {
    function DomainSet(data) {
      this.has = data;
    }

    DomainSet.ALL = '';

    DomainSet.domainAndParents = function (domain) {
      var result = {};
      var parts = domain.split('.');
      var nextDomain = parts[parts.length - 1];
      for (var i = parts.length - 1; i >= 0; --i) {
        result[nextDomain] = 1;
        if (i > 0) {
          nextDomain = parts[i - 1] + '.' + nextDomain;
        }
      }
      return result;
    };

    DomainSet.prototype = {
      clone: function () {
        return new DomainSet(angular.copy(this.has));
      },
      subtract: function (other) {
        for (var d in other.has) {
          this.has[d] = this._computedHas(d);
        }
        for (var d in this.has) {
          this.has[d] = this.has[d] && !other._computedHas(d);
        }
        this.has = this._optimizedDomains();
      },
      _optimizedDomains: function() {
        var newHas = {};
        newHas[DomainSet.ALL] = this.has[DomainSet.ALL];
        for (var d in this.has) {
          if (this.has[d] !== this._computedHas(parentDomainOf(d))) {
            newHas[d] = this.has[d];
          }
        }
        return newHas;
      },
      _computedHas: function (domain) {
        return this.has[domain] !== undefined ? this.has[domain] : this._computedHas(parentDomainOf(domain));
      }
    };

    function parentDomainOf(domain) {
      return domain.replace(/^.+?(?:\.|$)/, '');
    }

    return DomainSet;
  });