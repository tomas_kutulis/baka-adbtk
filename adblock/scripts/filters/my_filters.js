'use strict';

angular.module('adbtk')
  .constant('defaultSubscriptions', [
    { url: 'https://easylist-downloads.adblockplus.org/easylist.txt' },
    { url: 'http://margevicius.lt/easylistlithuania.txt' }
  ])
  .factory('myFilters', function ($http, storageService, filterNormalizer, defaultSubscriptions, Filter, SelectorFilter, PatternFilter, FilterSet, BlockingFilterSet) {
    var subscriptions = storageService.getObject('filter_lists'), blocking, hiding;
    if (subscriptions) {
      rebuild();
    } else {
      subscriptions = defaultSubscriptions;
      checkFilterUpdates();
    }

    function onSubscriptionChange() {
      storageService.setItem('filter_lists', subscriptions);
      rebuild();
    }

    function rebuild() {
      var texts = [];
      for (var i = 0; i < subscriptions.length; ++i) {
        texts.push(subscriptions[i].text);
      }
      texts = texts.join('\n').split('\n');

      var filters = splitByType(texts);
      hiding = FilterSet.fromFilters(filters.hiding);
      blocking = new BlockingFilterSet(FilterSet.fromFilters(filters.pattern), FilterSet.fromFilters(filters.whitelist));
    }

    function filterFromText(text) {
      return Filter.isSelectorFilter(text) ? new SelectorFilter(text) : PatternFilter.fromText(text);
    }

    function getUniqueTexts(texts) {
      var unique = {};
      for (var i = 0; i < texts.length; i++) {
        unique[texts[i]] = 1;
      }
      delete unique[''];
      return unique;
    }

    function splitByType(texts) {
      var filters = { hidingUnmerged: [], hiding: {}, exclude: {}, pattern: {}, whitelist: {} };
      fillFilters(filters, getUniqueTexts(texts));
      mergeHiding(filters);

      return filters;
    }

    function fillFilters(filters, unique) {
      for (var text in unique) {
        var filter = filterFromText(text);
        if (Filter.isSelectorExcludeFilter(text)) {
          setDefault(filters.exclude, filter.selector, []).push(filter);
        } else if (Filter.isSelectorFilter(text)) {
          filters.hidingUnmerged.push(filter);
        } else if (Filter.isWhitelistFilter(text)) {
          filters.whitelist[filter.id] = filter;
        } else {
          filters.pattern[filter.id] = filter;
        }
      }
    }

    function mergeHiding(filters) {
      for (var i = 0; i < filters.hidingUnmerged.length; i++) {
        var filter = filters.hidingUnmerged[i];
        var hider = SelectorFilter.merge(filter, filters.exclude[filter.selector]);
        filters.hiding[hider.id] = hider;
      }
    }

    function fetch_and_update(subscription) {
      $http.get(subscription.url).then(function (response) {
        subscription.text = filterNormalizer.getFilters(response.data);
        onSubscriptionChange();
      });
    }

    function checkFilterUpdates() {
      for (var i = 0; i < subscriptions.length; ++i) {
        fetch_and_update(subscriptions[i]);
      }
    }

    function getBlocking() {
      return blocking;
    }

    function getHiding() {
      return hiding;
    }

    return {
      getBlocking: getBlocking,
      getHiding: getHiding
    }
  });
